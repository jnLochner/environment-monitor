// EmonLibrary examples openenergymonitor.org, Licence GNU GPL V3

#include "EmonLib.h"                   // Include Emon Library
#include <Arduino.h>
#include <avr/wdt.h>
EnergyMonitor emon1;                   // Create an instance

void setup()
{  
  wdt_disable();
  delay(2L * 1000L);
  wdt_enable(WDTO_4S);
  wdt_reset();
  Serial.begin(9600);
  
  emon1.current(1, 22);             // Current: input pin, calibration.
  
}
int reset = 0;
void loop()
{
  double Irms = emon1.calcIrms(1480);  // Calculate Irms only
  
  //Serial.print(Irms*230.0);	       // Apparent power
  //Serial.print(" ");
  int serialByte = Serial.read();
  if(serialByte=='E')
    reset = 1;
  if(reset == 0)
    wdt_reset();
  Serial.print(Irms);		       // Irms
  Serial.println(" ");
}
