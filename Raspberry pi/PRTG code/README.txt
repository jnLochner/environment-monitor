these files should be placed on the raspberry pi at below mentioned location

/var/prtg/scripts

if directory does not exist, create it.

If placed in correct directory, a PRTG sensor can be set up with SSH.

*****************************************************************************
PRTG setup: (amused account is already created and server running)
*****************************************************************************
1. run PRTG network monitor
2. Go to devices and add new device
3. Add Raspberry pi IP at IPv4 Address/DNS Name 
4. uncheck inherit from under CREDENTIALS FOR LINUX/SOLARIS/MAC OS (SSH/WBEM) SYSTEMS
5. Add Raspberry pi user and password and press continue
6. Add new sensor and choose newll created device
7. Change name, choose the SSH SCRIPT sensor, and choose between humid.sh (humidity sensor) and temp.sh (temperature sensor) and press continue
8. If rapsberry pi is connected the senor should start.
9. The user can make any changes as needed.

