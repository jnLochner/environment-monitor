import serial
import time
from SerialComms import SerialComms
ser = SerialComms('/dev/ttyACM0',9600)
ser.open()

def getValue(message,mesLen,start):
	value = message[start]
	for i in range(1+start,mesLen):
		if message[i] == ' ':
			break
		pass
		value = value + message[i]
	return value

def getMeasurements():
	receive = ser.receive()
	received = len(receive)
	if received>=1:
		message = receive
		mesLen = len(message)
		curr = getValue(message,mesLen,0)
		print(curr)

while True:
	getMeasurements()
	time.sleep(1)
