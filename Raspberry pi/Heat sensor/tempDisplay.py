import os
import glob
import time
import sys
import Adafruit_DHT as dht
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import datetime

#fig = plt.figure()
#ax1 = fig.add_subplot(1,1,1)

log = open('temperature log', 'w+')

tempData = []
count = 0
displayGraph = False

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'


#run the commands and get returned string
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

#extract the temperature value from string
def read_temp():
    global tempData
    global count
    global displayGraph
    lines = read_temp_raw()
    
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.5)
        lines = read_temp_raw()
        
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos +2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 +32.0
        count = count + 1
        tempData.append(temp_c)
        displayGraph  = True
        return temp_c, temp_f


def animate(i):
    global count
    global tempData

    xvar = range(0,count,1)
    yvar = tempData

    ax1.clear()
    ax1.plot(xvar,yvar)

    c,f = read_temp()
    updateLog(str(c))
    print('temp_ds18b20:' + str(c))


def updateLog(temp):
    global log
    log = open('temperature log', 'a+')
    log.write('Time: ' + str(datetime.datetime.now()) + '       Temperature: ' + temp + '\n')
    log.close()
    
#ani = animation.FuncAnimation(fig, animate, xrange(1,200), interval=1000)
#plt.show()

#Main loop
while True:
    #DHT11 measured values
    humidity, temperature = dht.read_retry(dht.DHT11,17)	
    print 'temp_DHT11:' + str(temperature) + ' humid:' + str(humidity)

    #ds18b20 measured values
    c,f = read_temp()
   ## updateLog(str(c))
    print('temp_ds18b20:' + str(c))  
   ## time.sleep(1)


