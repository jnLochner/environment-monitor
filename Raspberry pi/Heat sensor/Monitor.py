import os
import glob
import time
import sys
import Adafruit_DHT as dht
import datetime
import serial
import gammu
from SerialComms import SerialComms
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

smsSent1 = False
smsSent2 = False

sm = gammu.StateMachine()
sm.ReadConfig()
sm.Init()

heatMessage = {
    'Text': 'Warning: Overheating', 
    'SMSC': {'Location': 1},
    'Number': '+27783082835',
}

powerMessage = {
    'Text': 'Warning: NO POWER', 
    'SMSC': {'Location': 1},
    'Number': '+27783082835',
}

ser = SerialComms('/dev/ttyACM0',9600)
ser.open()

log = open('temperature log', 'w+')

tempData = []
count = 0
displayGraph = False

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

#run the commands and get returned string
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

#extract the temperature value from string
def read_temp():
    global tempData
    global count
    global displayGraph
    lines = read_temp_raw()
    
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.5)
        lines = read_temp_raw()
        
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos +2:]
        temp_c = float(temp_string) / 1000.0
        count = count + 1
        tempData.append(temp_c)
        displayGraph  = True
        return temp_c

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def sendMail(body,subject):
	with open(body) as fp:
    		msg = MIMEText(fp.read())
 
	fromaddr = "fouriedylon@gmail.com"
	toaddr = "jnlochner@gmail.com"
	msg['From'] = fromaddr
	msg['To'] = toaddr
	msg['Subject'] = subject
 
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, "12345asd")
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()

def sendAlert(temp,current):
	global sm
	global smsSent1
	global smsSent2
	if isfloat(temp) and isfloat(current):

		if float(temp) >= 50.0 and not smsSent1:
			sm.SendSMS(heatMessage)
			sendMail('tempError.txt','ERROR:HEATING')
			print('WARM')
			smsSent1 = True

		if float(current) <= 0.09 and not smsSent2:
			sm.SendSMS(powerMessage)
			sendMail('currError.txt','ERROR:NO_POWER')
			print('Krag')
			smsSent2 = True
	
#Main loop
while True:
    #DHT11 measured values
##    humidity, temperature = dht.read_retry(dht.DHT11,17)
##    print 'Humidity: ' + str(humidity)

    #ds18b20 measured values
    t = read_temp()
    print('Temperature: ' + str(t))

    #read current
    c = ser.receive()
    cLength = len(c)
    if cLength >= 1:
    	print('Current: ' + c[0])
	sendAlert(t,c[0])
    print(' ')	
    #wait one second
    time.sleep(1)


