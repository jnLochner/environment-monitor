import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
    pullData = open("a.txt","r").read()
    dataArray = pullData.split('\n')
    xvar = []
    yvar = []
    for eachLine in dataArray:
        if len(eachLine)>1:
            x,y = eachLine.split(',')
            xvar.append(int(x))
            yvar.append(int(y))
            
    ax1.clear()
    ax1.plot(xvar,yvar)

ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.show()
