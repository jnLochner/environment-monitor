import serial
import time
from tkinter import *
from SerialComms import SerialComms
import serial.tools.list_ports
from datetime import datetime as dt
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from tkinter.scrolledtext import ScrolledText

#initilize serial communications
#ser.send('stream.off')
#ser.baudrate = 9600;
#ser.port = 'COM6'

#GUI
gui = Tk()
gui.title("Measurments")
gui.grid()

connected = False
ser = SerialComms('COM6', 19200)
start = 0;
streaming = False

currTotal = 0
currCount = 0

#Sends an encrypted email
def sendmail(alertname,alertmail):
    subject = alertname
    filename = alertmail

    with open(filename) as fp:
        msg = MIMEText(fp.read())
        
    fromaddr = "fouriedylon@gmail.com"
    toaddr = "jnlochner@gmail.com"
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = subject
     
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "12345asd")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)

    server.quit()
    

def stream():
    global start
    global streaming
    if streaming:
        start = dt.now()
        getMeasurements()
    gui.after(1000, stream)
            
        
def connect():
    global connected
    global ser
    if not connected:
        baud = baudrate_text.get()
        com = com_text.get()
        ser = SerialComms(com, baud)
        ser.open()
        connected = True
        btn_connect['text'] = 'Disconnect'

    elif connected:
        btn_connect['text'] = 'Connect'
        ser.close()
        connected = False
              

def getValue(message,mesLen,start):
    value = message[start]
    for i in range(1+start,mesLen):
        if message[i] == ' ':
            break
        pass
        value = value+message[i]    

    #print(value)     
    return value,i

def getMeasurements():
    receive = ser.receive()
    received = len(receive)
    global streaming
    streaming = True
    global currTotal
    global currCount
   # print(receive)
    if received>=1:
        message = receive[0]
        mesLen = len(message)
        #realPower = getValue(message,mesLen,0);
        #apparentPower = getValue(message,mesLen,0);
        #pf = getValue(message,mesLen,apparentPower[1]);
        #vol = getValue(message,mesLen,apparentPower[1]);
        curr = getValue(message,mesLen,0);
        a = float(curr[0])
        if a <=0.31:
            a = 0.0
        lbl_i2['text'] = str('{0:.2f}'.format(a))
##        currTotal = currTotal + float(curr[0])    
##        currCount = currCount + 1
        
        #pf = getValue(message,mesLen,curr[1]);
       # lbl_real2['text'] = realPower[0]
       # lbl_apP2['text'] = apparentPower[0]
       # lbl_PF2['text'] = pf[0]
##        if currCount >= 0:
##            lbl_i2['text'] = "{0:.2f}".format(currTotal/currCount)
##            currCount = 0
##            currTotal = 0
##            print('ja')
       # lbl_v2['text'] = vol[0]

def click():
    global streaming
    if btn_measure['text'] == 'Start':
        getMeasurements()
        btn_measure['text'] = 'Stop'
    else:
        streaming = False
        btn_measure['text'] = 'Start'
    pass

def setBaudrate(baud):
    ser.baudrate = baud

def setCom(com):
    ser.port = com

def email_window():
    window = Toplevel(gui)
    window.title("Email")
    window.grid()
    window.resizable(False, False)

    login_l = Label(window, text = '        LOGIN')
    login_l.grid(column = 2, row = 0, padx=3, pady=3, sticky = W)
    email_l = Label(window, text = 'Email:')
    email_l.grid(column = 1, row = 1, padx=3, pady=3, sticky = E)
    email_text = Entry(window,width=20)
    email_text.grid(column = 2, row = 1, padx=3, pady=3, sticky = W)
    pass_l = Label(window, text = 'Password:')
    pass_l.grid(column = 1, row = 2, padx=3, pady=3, sticky = E)
    pass_text = Entry(window,width=20)
    pass_text.grid(column = 2, row = 2, padx=3, pady=3, sticky = W)
    fr1 = Frame(window)
    fr1.grid(column=0, row=3, rowspan=1, columnspan=1, padx=3, pady=3, sticky=N+S+E+W)
    to_l = Label(fr1, text = 'To: ')
    to_l.grid(column = 0, row = 0, padx=3, pady=3, sticky = W)
    to_text = Entry(fr1,width=20)
    to_text.grid(column = 1, row = 0, padx=3, pady=3, sticky = W+E)
    subject_l = Label(fr1, text = 'Subject: ')
    subject_l.grid(column = 0, row = 1, padx=3, pady=3, sticky = E)
    subject_text = Entry(fr1,width=20)
    subject_text.grid(column = 1, row = 1, padx=3, pady=3, sticky = W+E)
    message_l = Label(window, text = 'Text:')
    message_l.grid(column = 0, row = 6, padx=3, pady=3, sticky = W)
    message_text = ScrolledText(window)
    message_text.grid(column = 0, row = 7, rowspan=1, columnspan=3)
 

#frames
fc = Frame(gui)
fc.grid(column=0, row=0, rowspan=1, columnspan=4, padx=3, pady=3, sticky=N+S+E+W)

fl = Frame(gui)
fl.grid(column=0, row=2, rowspan=6, columnspan=4, padx=3, pady=3, sticky=N+S+E+W)
    
Baudrate_lb = Label(fc, text = 'Baudrate')
Baudrate_lb.grid(column = 0, row = 0, padx=3, pady=3, sticky = E)
baudrate_text = Entry(fc,width=5)
baudrate_text.grid(column = 1, row = 0, padx=3, pady=3, sticky = W+E)

com_lb = Label(fc, text = 'Com Port')
com_lb.grid(column = 2, row = 0, padx=3, pady=3)
com_text = Entry(fc,width=7)
com_text.grid(column = 3, row = 0,sticky = W+E,padx=3, pady=3)

lbl_blank = Label(fl, text='                   ')
lbl_blank.grid(column=0, row=0, columnspan=1)

lbl_v = Label(fl, text='Voltage:')
lbl_v.grid(column=1, row=2, padx=3, pady=3, sticky=W)
lbl_v2 = Label(fl, text='0 V')
lbl_v2.grid(column=2, row=2, padx=3, pady=3, sticky=W)
lbl_i = Label(fl, text='Current:')
lbl_i.grid(column=1, row=3, padx=3, pady=3, sticky=W)
lbl_i2 = Label(fl, text='0 A')
lbl_i2.grid(column=2, row=3, padx=3, pady=3, sticky=W)
lbl_real = Label(fl, text='Real Power:')
lbl_real.grid(column=1, row=4, padx=3, pady=3, sticky=W)
lbl_real2 = Label(fl, text='0 W')
lbl_real2.grid(column=2, row=4, padx=3, pady=3, sticky=W)
lbl_apP = Label(fl, text='Apparent Power:')
lbl_apP.grid(column=1, row=5, padx=3, pady=3, sticky=W)
lbl_apP2 = Label(fl, text='0 var')
lbl_apP2.grid(column=2, row=5, padx=3, pady=3, sticky=W)
lbl_PF = Label(fl, text='Power Factor:')
lbl_PF.grid(column=1, row=6, padx=3, pady=3, sticky=W)
lbl_PF2 = Label(fl, text='0')
lbl_PF2.grid(column=2, row=6, padx=3, pady=3, sticky=W)

btn_measure = Button(fl, text = 'Start', command = click)
btn_measure.grid(column=1, row=7, columnspan = 2, padx=3, pady=3, sticky=N+S+E+W)

btn_connect = Button(gui, text = 'Connect', command = connect, width=9)
btn_connect.grid(column=4, row=0, columnspan = 2, padx=3, pady=3, sticky=N+S+E+W)

btn_email = Button(gui, text = 'Send Mail', command = email_window, width=9)
btn_email.grid(column=4, row=7, columnspan = 2, padx=3, pady=3, sticky=E+S)


gui.resizable(False, False)
gui.after(1, stream())
gui.mainloop()
